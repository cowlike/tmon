﻿module Tests

open System
open Xunit
open Swensen.Unquote
open TMon
open TMon.Utils

/// A helper class to verify that Dispose() was called
[<AllowNullLiteralAttribute>]
type MyDisposable() =
    let mutable disposed = false
    member this.isDisposed() = disposed
    interface System.IDisposable with member this.Dispose() = disposed <- true

[<Fact>]
let ``Maybe bind on Just`` () =
    Just 5 >>= (((*) 10) >> return') =! Just 50

[<Fact>]
let ``Maybe fmap on Just`` () =
    ((*) 10) <!> Just 5 =! Just 50

[<Fact>]
let ``Maybe Ap on Just`` () =
    let f x y = x * 100 + y * 10
    f <!> Just 5 <*> Just 2 =! Just 520

[<Fact>]
let ``Maybe comp expr`` () =
    do' {
        return! Just "x"
    } =! Just "x"

[<Fact>]
let ``Maybe with use`` () =
    let d1 = new MyDisposable()
    let d2 = new MyDisposable()

    let s = do' {
        let! x = Just "x"
        use d = d1
        use! e = Just d2
        return x
    }
    (s, d1.isDisposed(), d2.isDisposed()) =! (Just "x", true, true)

[<Fact>]
let testIsJust () =
    [Just 3; Nothing; Just 4]
    |> List.filter isJust
    |> List.length
    |> (=!) 2

[<Fact>]
let ``Result bind Success`` () =
    Success 5 >>= (((*) 10) >> return') =! Success 50

[<Fact>]
let ``Result fmap Success`` () =
    ((*) 10) <!> Success 5 =! Success 50

[<Fact>]
let ``Result Ap Success`` () =
    let f x y = x * 100 + y * 10
    f <!> Success 5 <*> Success 2 =! Success 520

[<Fact>]
let ``Result Ap Fail`` () =
    let f x y = x * 100 + y * 10
    f <!> Success 5 <*> Fail "oops" =! Fail "oops"

[<Fact>]
let ``Result with use`` () =
    let d1 = new MyDisposable()
    let d2 = new MyDisposable()

    let s = do' {
        use d = d1
        let! x = Fail "oops"
        use e = d2
        return "test"
    }
    (s, d1.isDisposed(), d2.isDisposed()) =! (Fail "oops", true, false)

[<Fact>]
let ``Type conversion in do`` () =
    do' {
        let! x = Success 10
        let! y = None |> Result.fromOption ""
        return x + y
    } =! Fail ""

[<Fact>]
let ``defaultValue uses default on Fail`` () =
    Result.defaultValue 0 (Fail "Oops") =! 0

[<Fact>]
let ``defaultValue uses wrapped value on Success`` () =
    Result.defaultValue 0 (Success 1) =! 1

[<Fact>]
let testIsSuccess () =
    [Success 3; Fail ""; Success 4]
    |> List.filter isSuccess
    |> List.length
    |> (=!) 2

[<Fact>]
let toChoice () =
    Success 3
    |> Result.toChoice
    |> function Choice1Of2 x -> x = 3 | _ -> false
    |> (=!) true

[<Fact>]
let fromChoice () =
    Choice1Of2 3
    |> Result.fromChoice
    |> function Success x -> x = 3 | _ -> false
    |> (=!) true

[<Fact>]
let ``Loop over Maybe`` () =
    let rec loop total = do' {
        if (total >= 1_000_000) then
            return! Just total
        else
            return! loop (total + 1)
    }
    loop 0 =! Just 1_000_000

[<Fact>]
let ``Loop over Async`` () =
    let rec loop total = async {
        if (total >= 10_000_000) then
            return total
        else
            return! loop (total + 1)
    }
    let result = loop 0 |> Async.RunSynchronously
    result =! 10_000_000


/// AR tests for map, apply & bind

[<Fact>]
let ``AR map`` () =
    let x:AR<int,string> = return' 10

    string <!> x |> AR.run =! Success "10"

let getAsyncCanFail shouldFail (sleepTime: int) x = AR <| async {
    do! Async.Sleep sleepTime
    return if shouldFail then Fail "oops" else Success x
}

let getAsync sleepTime x = getAsyncCanFail false sleepTime x

[<Fact>]
let ``AR map with apply`` () =
    let v3: AR<int,string> = getAsync 0 3
    let v5: AR<int,string> = getAsync 0 5
    (*) <!> v3 <*> v5 |> AR.run =! Success 15

[<Fact>]
let ``AR map with apply is fast`` () =
    let v3: AR<int,string> = getAsync 100 3
    let v5: AR<int,string> = getAsync 100 5

    let (_, elapsed) = runtimeMillis (fun () -> (*) <!> v3 <*> v5 |> AR.run)
    elapsed <! int64 120

[<Fact>]
let ``AR apply`` () =
    let f: AR<(int -> int -> int -> int),string> = return' (fun a b c -> a * b * c)
    let v3: AR<int,string> = getAsync 0 3
    let v5: AR<int,string> = getAsync 0 5

    f <*> v3 <*> v5 <*> v3 |> AR.run =! Success 45

[<Fact>]
let ``AR apply fails`` () =
    let f: AR<(int -> int -> int),string> = return' (*)
    let v3: AR<int,string> = getAsync 1000 3
    let v5: AR<int,string> = AR.fail "Oops"

    f <*> v3 <*> v5 |> AR.run =! Fail "Oops"

[<Fact>]
let ``AR bind is sequential`` () =
    let v1:AR<int,string> = getAsync 1000 10
    let v2:AR<int,string> = getAsync 1000 5

    let foo = (v1 >>= (fun x -> v2 >>= (fun y -> return' (x + y)))) |> AR.run
    foo =! Success 15

[<Fact>]
let ``AR bind fails fast`` () =
    let v1:AR<int,string> = AR.fail "Oops"
    let v2:AR<int,string> = getAsync 10000 3

    //using do' notation
    let (foo, elapsed1) = runtimeMillis (fun () ->
        do' {
            let! v1 = v1
            let! v2 = v2
            return (v1 + v2)
        } |> AR.run)

    //using bind operator
    let f () = v1 >>= (fun x -> v2 >>= (fun y -> return' (x + y))) |> AR.run

    let (bar, elapsed2) = runtimeMillis f

    elapsed1 <! int64 50
    elapsed2 <! int64 50
    foo =! Fail "Oops"
    bar =! Fail "Oops"

[<Fact>]
let ``AR.sleep during computation`` () =
    let (v, elapsed) = runtimeMillis (fun () ->
        do' {
            let! v1 = getAsync 10 10
            do! AR.sleep 500
            let! v2 = getAsync 10 5
            return (v1 + v2)
        } |> AR.run)

    elapsed >! int64 500
    v =! Success 15

[<Fact>]
let ``AR with use`` () =
    let d1 = new MyDisposable()

    let v =
        do' {
            let! v1 = getAsync 10 10
            use d = d1
            return v1 * 2
        } |> AR.run
    (v, d1.isDisposed()) =! (Success 20, true)

[<Fact>]
/// Run some AR sequences in multiple ways. Not much of a test, more like a demo
let ``AR sequence computation`` () =
    let good = getAsync 100
    let bad = getAsyncCanFail true 100

    let double x = x * 2
    let doubleAR (x: AR<int,string>) = x >>= (double >> return')
    let doubleMany a b c d e = [double a; double b; double c; double d; double e]
    let xs = List.map good [1..5]
    let xs' = List.map bad [1..5]
    let xsWrapped = xs |> List.sequenceAR
    let t1 = runtimeMillis (fun () -> (List.map double <!> xsWrapped) |> AR.run)
    let t2 = runtimeMillis
                (fun () -> (List.map (fun x -> double <!> x) xs)
                                               |> AR.runParallel
                                               |> List.ofSeq
                                               |> List.sequenceResultA)
    let t3 = runtimeMillis (fun () -> List.map doubleAR xs |> List.sequenceAR |> AR.run)
    let t4 = runtimeMillis (fun () -> List.map doubleAR xs' |> List.sequenceAR |> AR.run)
    let t5 = runtimeMillis
                (fun () -> doubleMany
                           <!> good 1
                           <*> good 2
                           <*> good 3
                           <*> good 4
                           <*> good 5 |> AR.run)
    let t6 = runtimeMillis
                (fun () -> doubleMany
                           <!> good 1
                           <*> good 2
                           <*> bad 3
                           <*> good 4
                           <*> good 5 |> AR.run)
    printfn "run: %A\nrunParallel: %A\nrunBind: %A" t1 t2 t3
    printfn "fails: %A\napply: %A\napply w fail: %A" t4 t5 t6
    snd t1 + snd t2 <=! snd t3

[<Fact>]
let ``Another timer test returning List`` () =
    let (v, _) = runtimeMillis (fun () -> [1..5])
    List.sum v =! 15
