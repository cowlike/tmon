# Some monad helpers and a few utilities in **F#**

targets **netstandard2.0**

[Source code](https://gitlab.com/cowlike/tmon)
The tests in `Tests/Tests.fs` show how to call the various functions and operators.

* Version **0.0.7** add MIT license to package

* Version **0.0.6** add

    ```fsharp
    Result.fromChoice : Choice<'a,'b> -> Result<'a,'b>
    Result.toChoice : Result<'a,'b> -> Choice<'a,'b>
    //more tests
    ```

* Version **0.0.5** add implementation of `Using`

* Version **0.0.4** add

    ```fsharp
    Maybe.defaultValue : Maybe<'a> -> 'a
    Maybe.isJust: Maybe<'a> -> bool
    Result.isSuccess: Result<'a,'b> -> bool
    //more tests
    ```

* Version **0.0.3** add `AR.runParallel` and some examples using AR in `Tests/Tests.fs`

* Version **0.0.2** adds `Result.defaultValue`
